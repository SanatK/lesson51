package kg.attractor.microgram2.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
@Data
@Document
public class Artist {
    @Id
    private String id;
    @Indexed
    private String fullName;
    public Artist(String id, String fullName){
        this.id = id;
        this.fullName = fullName;
    }
    public String toString(){
        String fmt = "Artist id: %s, Artist fullName: %s\n";
        return String.format(fmt, id, fullName);
    }

}
