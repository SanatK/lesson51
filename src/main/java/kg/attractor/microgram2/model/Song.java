package kg.attractor.microgram2.model;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "songs")
public class Song {
    @Id
    private String id;
    @Indexed
    private String name;
    private Artist artist;
    public Song(String id, String name, Artist artist){
        this.id = id;
        this.name = name;
        this.artist = artist;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Artist getArtist() {
        return artist;
    }

    public void setArtist(Artist artist) {
        this.artist = artist;
    }
    public String toString(){
        String fmt = " ------- Song id: %s, Song name:%s, Song author: %s -------- \n";
        return  String.format(fmt, id, name, artist);
    }
}
