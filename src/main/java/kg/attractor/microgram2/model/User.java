package kg.attractor.microgram2.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Document
public class User {
    @Id
    private String email;
    private String name;
    private String password;
    private int postsCounter;
    private int subscribesCounter;
    private int subscribersCounter;
    @DBRef
    private List<Publication> pub = new ArrayList<>();

    public User(String email, String name, String password) {
        Objects.requireNonNull(email);
        Objects.requireNonNull(name);
        Objects.requireNonNull(password);
        this.email = email;
        this.name = name;
        this.password = password;
    }
    public String toString(){
        String fmt = "%s, %s, %s";
        return String.format(fmt, email, name, password);
    }

}
