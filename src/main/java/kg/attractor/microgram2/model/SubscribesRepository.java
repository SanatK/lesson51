package kg.attractor.microgram2.model;

import org.springframework.data.repository.CrudRepository;

public interface SubscribesRepository extends CrudRepository<Subscribes, String> {
}
