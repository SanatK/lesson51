package kg.attractor.microgram2.model;

import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, String> {
    public User findByName(String name);
    public User findByPassword(String password);
}
