package kg.attractor.microgram2.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
@Data
@Document
public class Album {
    @Id
    private String id;
    @Indexed
    private String name;
    @DBRef
    private List<Song> songs = new ArrayList<>();
    private List<Artist> artists;
    private LocalDateTime time;

    public Album(String id, String name, List<Song> songs, List<Artist> artists, LocalDateTime time){
        this.id = id;
        this.name = name;
        this.songs = songs;
        this.artists = artists;
        this.time = time;
    }
    public String toString(){
        String fmt = " --------- Album id: %s, Album name: %s\n, Album songs: %s\n, Album authors: %s\n ----------- ";
        return String.format(fmt, id, name, songs, artists);
    }
}
