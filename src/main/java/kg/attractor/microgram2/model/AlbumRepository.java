package kg.attractor.microgram2.model;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AlbumRepository extends CrudRepository<Album, String> {
    public Album findByName(String name);

}
