package kg.attractor.microgram2.model;

import org.springframework.data.repository.CrudRepository;

public interface ArtistRepository extends CrudRepository<Artist, String> {
}
