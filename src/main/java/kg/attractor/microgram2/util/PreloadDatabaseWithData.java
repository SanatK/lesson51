package kg.attractor.microgram2.util;
import kg.attractor.microgram2.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;


@Configuration
public class PreloadDatabaseWithData {

public final UserRepository ur;
public final CommentRepository cr;
public final AlbumRepository ar;
public static Artist artist = new Artist("001", "role Model");
public static Artist artist2 = new Artist("002", "Eminem");


public  PreloadDatabaseWithData(UserRepository ur, CommentRepository cr, AlbumRepository ar){
    this.ar = ar;
    this.ur = ur;
    this.cr = cr;
    List<Artist> artists = new ArrayList<>();
    artists.add(artist);
    
    artists.add(artist2);
    LocalDateTime time = LocalDateTime.now();

    Album album = new Album("001", "AAA", getSongs(), artists, time);
    System.out.println("------------------------------------------------------------");
    System.out.println(album);
    System.out.println("------------------------------------------------------------");

}
    public List<Song> getSongs(){
                List<Song> s = new ArrayList<>();
                s.add( new Song("001", "Minimal", artist));
                s.add(new Song ("002", "Monster", artist2));
                s.add(new Song ("003", "Numb", artist));
                s.add(new Song ("004", "Sad", artist2));
                s.add(new Song ("005", "Scar Tissue", artist));
                s.add(new Song ("006", "Honest", artist2));
                return s;
    }
    @Bean
    CommandLineRunner initUserDatabase(UserRepository repository) {

        repository.deleteAll();

        return (args) -> Stream.of(users())
                .peek(System.out::println)
                .forEach(repository::save);
    }

    @Bean
    CommandLineRunner initCommentDatabase(CommentRepository repository) {

        repository.deleteAll();

        return (args) -> Stream.of(comments())
                .peek(System.out::println)
                .forEach(repository::save);
    }


    private User[] users() {
        return new User[]{
                new User ("Sana@gmail.com", "Sana", "12345"),
                new User ("Nazira@gmail.com", "Nazira", "20211"),
                new User ("Azer@gmail.com", "Azer", "realmadrid"),
                new User ("Jama@gmail.com", "Jama", "2020"),
                new User ("Bema@gmail.com", "Bema", "password"),
                new User ("Keka@gmail.com", "Keka", "embr.k")};
    }
    private Comment[] comments() {
        return new Comment[]{
                new Comment ("1", "SanaK@gmail.com", "It was good day!", getTime()),
                new Comment ("2", "Bema@gmail.com", "Yeah, perfect day!", getTime()),
                new Comment ("3", "Keka@gmail.com", "Hello world", getTime()),
                new Comment ("4", "SanaK@gmail.com", "Somebody once told me", getTime()),
                new Comment ("5", "Nazira@gmail.com", "We are the champions", getTime()),
                new Comment("6", "Bema@gmail.com", "Blade Runner 2049", getTime())};
    }
    private LocalDateTime getTime() {
        LocalDateTime time = LocalDateTime.now();
        Random rnd = new Random();
        int x = rnd.nextInt(100);
        time.minusHours(x);
        return time;
    }
}
