package kg.attractor.microgram2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Microgram2Application {

    public static void main(String[] args) {
        SpringApplication.run(Microgram2Application.class, args);
    }

}
